const express = require('express');
const http = require('http')
const { v4: uuidv4 } = require('uuid');
const app = express();
const port = require('./config.js');

const fs = require('fs')
const requestId = require('express-request-id')

app.use(requestId())

//  middleware function 
const requestLogger = (req, res, next) => {

  if(req.originalUrl ==='/favicon.ico')
  {
    next();
  }else
 {
    const loggingData =`${new Date().toISOString()} - Request ID: ${req.id} - Method : ${req.method} - Url : ${req.originalUrl}\n`;;
      fs.appendFile('request.log', loggingData, (err) => {
        if (err) 
        {
          console.error('Error writing to request log:', err);
        }
    });
    next();
  }
};

app.use(requestLogger);

app.get('/html', (req, res) => {
    res.send(`
        <!DOCTYPE html>
        <html>
          <head>
          </head>
          <body>
            <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
            <p> - Martin Fowler</p>
          </body>
        </html>
    `);
});

app.get('/json', (req, res) => {
    res.send(`
    {
        "slideshow": {
          "author": "Yours Truly",
          "date": "date of publication",
          "slides": [
            {
              "title": "Wake up to WonderWidgets!",
              "type": "all"
            },
            {
              "items": [
                "Why <em>WonderWidgets</em> are great",
                "Who <em>buys</em> WonderWidgets"
              ],
              "title": "Overview",
              "type": "all"
            }
          ],
          "title": "Sample Slide Show"
        }
      }
      `   
    )       
});

app.get('/uuid', (req, res) => {
  //  res.setHeader('Content-type', 'application/json');
    res.json(
      {
         uuid: uuidv4()
      });
});

app.get('/status/:status', (req, res,next) => {
    
    const status = Number(req.params.status);
    let statusCode = http.STATUS_CODES[status];

    if (statusCode === undefined || status === undefined) 
    {
     
      next(
        {
           message: "Bad status request",
           status : 404
        });
    }
    else 
    {
       res.statusCode = Number(status);
       res.send(
        {
           message: status
        });
    }

  })

app.get('/delay/:time', (req, res,next) => {

    const time = parseInt(req.params.time);
    if (isNaN(time) || time < 0) 
    {
        next({
           message :'Invalid',
           status :400
        })
    } else 
    {
        setTimeout(() => {
           res.status(200).json({ 'Response after': time });
        }, time * 1000);
    }
});

app.get('/logs', (req, res) => {
  fs.readFile('request.log', 'utf8', (err, data) => {
    if (err)
     {
        console.error('Error reading request log:', err);
        res.status(500).send({"Error" : "Internal Server Error"});
    } else 
    {
       res.send(data);
    }
  });
});

app.use((req, res) => {

    res.status(400).send('<h1>Main Page<h1>');
});

//    error handler
app.use((err,req, res, next)=>{
  console.error('Error occurred:', err);

  res.status(err.status).json({ error: err.message });
})

app.listen(4000, () => {
    console.log(`${4000} port is running`);
});
